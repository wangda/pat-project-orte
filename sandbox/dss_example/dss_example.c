#include <stdio.h>
#include "opal/dss/dss_types.h"
#include "opal/dss/dss.h"
#include "orte/util/proc_info.h"

int main(int argc, char* argv[]) {
  opal_buffer_t* buffer;
  buffer = OBJ_NEW(opal_buffer_t);

  int cnt = 1;
  int32_t i = 123;
  char* str = "hello world";
  long k = 333L;
  orte_process_name_t name;
  name.jobid = 12345;
  name.vpid = 54321;

  orte_init(&argc, &argv, ORTE_PROC_NON_MPI);

  // pack data
  opal_dss.pack(buffer, &i, 1, OPAL_INT32);
  opal_dss.pack(buffer, &str, 1, OPAL_STRING);
  opal_dss.pack(buffer, &k, 1, OPAL_INT64);
  opal_dss.pack(buffer, &name, 1, ORTE_NAME);

  printf("bytes used:%lu.\n", buffer->bytes_used);

  // unpack data
  int read_i;
  char* read_str;
  long read_k;
  orte_process_name_t read_name;
  opal_dss.unpack(buffer, &read_i, &cnt, OPAL_INT32);
  opal_dss.unpack(buffer, &read_str, &cnt, OPAL_STRING);
  opal_dss.unpack(buffer, &read_k, &cnt, OPAL_INT64);
  opal_dss.unpack(buffer, &read_name, &cnt, OPAL_INT64);

  printf("read i:%d.\n", read_i);
  printf("read str:%s.\n", read_str);
  printf("read k:%ld.\n", read_k);
  printf("read jobid:%d, vpid:%d.\n", read_name.jobid, read_name.vpid);

  OBJ_RELEASE(buffer);
}
