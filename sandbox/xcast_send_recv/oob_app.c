#include <stdio.h>
#include <signal.h>
#include <math.h>

#include "orte/util/proc_info.h"
#include "orte/util/name_fns.h"
#include "orte/runtime/orte_globals.h"

#include "orte/mca/rml/rml.h"
#include "orte/mca/errmgr/errmgr.h"
#include "orte/runtime/runtime.h"

#define MY_TAG 12345

bool msg_recvd = 0;

static void recv_cb(int status, orte_process_name_t* sender, 
    opal_buffer_t* buffer, orte_rml_tag_t tag, void* cbdata) {
    opal_output(0, "recved msg, sender jobid:%d, vpid:%d", sender->jobid, sender->vpid);
    orte_rml.send_buffer(sender, buffer, tag, 0);
    opal_output(0, "send msg to jobid:%d, vpid:%d, tag:%d", sender->jobid, sender->vpid, tag);
    msg_recvd = 1;
}

int main(int argc, char* argv[]) {
    orte_init(&argc, &argv, ORTE_PROC_NON_MPI);

    orte_rml.recv_buffer_nb(ORTE_NAME_WILDCARD, MY_TAG, ORTE_RML_NON_PERSISTENT, recv_cb, NULL);

    ORTE_PROGRESSED_WAIT(msg_recvd, 0, 1);

    orte_finalize();
    return 0;
}
