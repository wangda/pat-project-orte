#include <stdio.h>
#include <signal.h>
#include <math.h>

#include "orte/util/proc_info.h"
#include "orte/util/name_fns.h"
#include "orte/runtime/orte_globals.h"

#include "orte/mca/rml/rml.h"
#include "orte/mca/plm/plm.h"
#include "orte/mca/errmgr/errmgr.h"

#include "orte/runtime/runtime.h"

#define MY_TAG 12345
static int num_msg_recvd = 0;

static void recv_cb(int status, orte_process_name_t* sender,
    opal_buffer_t* buffer, orte_rml_tag_t tag, void* cbdata)
{
    num_msg_recvd++;

    opal_output(0, "recieved msg, sender jobid:%d, vpid:%d", sender->jobid, sender->vpid);
    
    // set another callback ASAP
    orte_rml.recv_buffer_nb(ORTE_NAME_WILDCARD, MY_TAG, ORTE_RML_NON_PERSISTENT,
        recv_cb, NULL);
}

int main(int argc, char* argv[]) {
    int rc;
    orte_job_t* jdata;
    orte_app_context_t* app;
    orte_vpid_t i;
    char cwd[1024];

    if (argc != 3) {
        fprintf(stderr, "oob_hnp exe_name proc_num.\n");
        return -1;
    }

    if (0 > (rc = orte_init(&argc, &argv, ORTE_PROC_NON_MPI))) {
        fprintf(stderr, "cannot init orte, rc%d\n", rc);
        return rc;
    }

    /* setup job obj */
    jdata = OBJ_NEW(orte_job_t);
    // jdata->controls set
    
    /* create app context */
    app = OBJ_NEW(orte_app_context_t);
    app->app = strdup(argv[1]);
    opal_argv_append_nosize(&app->argv, strdup(argv[1]));
    app->num_procs = atoi(argv[2]);

    getcwd(cwd, sizeof(cwd));
    app->cwd = strdup(cwd);
    app->user_specified_cwd = false;

    /* add it to job_t */
    opal_pointer_array_add(jdata->apps, app);
    jdata->num_apps = 1;

    /* launch job */
    opal_output(0, "before launch");
    if (ORTE_SUCCESS != (rc = orte_plm.spawn(jdata))) {
        ORTE_ERROR_LOG(rc);
        orte_finalize();
        return -1;
    }
    opal_output(0, "after launch");

    /* send msg to launched apps one by one */
    
    // init msg
    orte_process_name_t peer;
    peer.jobid = jdata->jobid;

    opal_buffer_t buf;
    OBJ_CONSTRUCT(&buf, opal_buffer_t);
    char* msg = (uint8_t*)malloc(10);
    opal_dss.pack(&buf, msg, 10, OPAL_BYTE);

    // listen to recieve
    orte_rml.recv_buffer_nb(ORTE_NAME_WILDCARD, MY_TAG, ORTE_RML_NON_PERSISTENT, recv_cb, NULL);

    // send buffer
    for (i = 0; i < app->num_procs; i++) {
        peer.vpid = i;
      if (0 > (rc = orte_rml.send_buffer(&peer, &buf, MY_TAG, 0))) {
          ORTE_ERROR_LOG(rc);
      }
    }
    
    opal_output(0, "after all send");
    ORTE_PROGRESSED_WAIT(num_msg_recvd == app->num_procs, 0, 1);
    opal_output(0, "successfully recved all responses");

    orte_finalize();
    return 0;
}


